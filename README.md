# NandSolo
The purpose of this project is to build a computer from scratch.
This includes building:

+   all the hardware using a basic Nand gate in HDL.

+   the whole computer architecture: ALU / CPU design and implementation, memory design,...

+   the Operating System written in C.

+   the assembler written in OCaml or Haskell.

+   virtual machine and compiler for the high level, object-oriented language written in C.

All those parts are described in _The Elements of Computing Systems_.

Find the book at [nand2tetris.com](http://www.nand2tetris.org/)