// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Mux8Way16.hdl

/**
 * 8-way 16-bit multiplexor.
 * out = a if sel==000
 *       b if sel==001
 *       etc.
 *       h if sel==111
 */

CHIP Mux8Way16 {
    IN a[16], b[16], c[16], d[16],
       e[16], f[16], g[16], h[16],
       sel[3];
    OUT out[16];

    PARTS:
    // Selection between a, b, c and d.
    Not(in=sel[0], out=nsel0);
    Not(in=sel[1], out=nsel1);
    Not(in=sel[2], out=nsel2);
    And(a=nsel1,b=nsel2, out=nsel12);
    And(a=nsel12, b=sel[0], out=selab);
    Mux16(a=a,b=b, sel=selab, out=ab);
    And(a=sel[1], b=nsel2, out=sel12);
    And(a=sel12, b=sel[0], out=selcd);
    Mux16(a=c, b=d, sel=selcd, out=cd);
    Mux16(a=ab, b=cd, sel=sel[1], out=out1);
    
    // Selection between e, f, g, h.
    And(a=nsel1, b=sel[2], out=sel21);
    And(a=sel21, b=sel[0], out=selef);
    Mux16(a=e, b=f, sel=selef, out=ef);
    And(a=sel[1], b=sel[2], out=sel3);
    And(a=sel3, b=sel[0], out=selgh);
    Mux16(a=g, b=h, sel=selgh, out=gh);
    Mux16(a=ef, b=gh, sel=sel[1], out=out2);

    Mux16(a=out1, b=out2, sel=sel[2], out=out);
    }
